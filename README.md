
# Ponyscape Sandbox Issues

It's a place where you can track, open, comment issues related to Ponyscape Sandbox Server

**[Start the journey on the Ponyscape](steam://connect/198.50.159.125:27017 "Connect to the server")**

Mind to subscribe to all events, by clicking on *notifications* button that named ```Global``` (by default), and selecting option what you would like.